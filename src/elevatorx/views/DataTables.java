package elevatorx.views;

import elevatorx.model.ElevatorTrip;
import elevatorx.model.TargetFloor;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Konglie on 7/17/2016.
 */
public class DataTables extends JPanel {
    JTabbedPane body;
    private DefaultTableModel ioModel;
    private DefaultTableModel procModel;
    private JScrollPane procPane;
    private JScrollPane ioPane;

    public DataTables(){
        super(new BorderLayout());
        buildGUI();
    }

    private void buildGUI(){
        body = new JTabbedPane();

        // table INPUT
        ioModel = new DefaultTableModel(null, new String[]{"INPUT", "OUTPUT", "TargetFloor"}){
            @Override
            public boolean isCellEditable(int x, int y){
                return false;
            }
        };
        JTable tinp = new JTable(ioModel);
        tinp.getColumnModel().getColumn(2).setMinWidth(0);
        tinp.getColumnModel().getColumn(2).setMaxWidth(0);
        tinp.getColumnModel().getColumn(2).setPreferredWidth(0);
        tinp.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,int row,int col) {
                Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
                TargetFloor tf = (TargetFloor) table.getValueAt(row, 2);
                c.setForeground(Color.BLACK);
                if(tf.isArrived()){
                    c.setForeground((col == 0) ? Color.BLUE : Color.ORANGE);
                } else if(tf.isMoving()){
                    c.setFont(c.getFont().deriveFont(Font.BOLD));
                }

                return c;
            }
        });

        ioPane = new JScrollPane(tinp);
        body.addTab("I/O", ioPane);

        // table Process
        procModel = new DefaultTableModel(null, new String[]{"No", "S", "X2", "X4"}){
            @Override
            public boolean isCellEditable(int x, int y){
                return false;
            }
        };
        JTable t2 = new JTable(procModel);
        procPane = new JScrollPane(t2);
        body.addTab("PROCESS", procPane);

        add(body, BorderLayout.CENTER);
    }

    public void renderIO(final ArrayList<TargetFloor> tfs, final ArrayList<ElevatorTrip> trips){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ioModel.setRowCount(0);

                for(Iterator<TargetFloor> it = tfs.iterator(); it.hasNext();){
                    TargetFloor tf = it.next();

                    // katanya yang sudah dikerjakan gak perlu ditampilkan lagi
                    if(tf.isArrived()){
                        it.remove();
                        continue;
                    }

                    ioModel.addRow(new Object[]{
                            ((tf.getDirection() == 1) ? "Naik" : "Turun") + " dari " + tf.getFromFloor(),
                            (tf.getToFloor() <= 0) ? "" : tf.getToFloor() + "",
                            tf
                    });
                }

                procModel.setRowCount(0);
                int i = 1;

                // tanda centang
                String checkMark = "<html>&#10004;</html>";

                for(Iterator<ElevatorTrip> it = trips.iterator(); it.hasNext(); )
                {
                    ElevatorTrip trip = it.next();
                    ElevatorTrip.ElevatorSpeed speed = trip.getSpeed();
                    procModel.addRow(new Object[]{
                            i++,

                            // berikan tanda centang pada kolom yang sesuai
                            (speed == ElevatorTrip.ElevatorSpeed.Standard) ? checkMark : "",
                            (speed == ElevatorTrip.ElevatorSpeed.X2) ? checkMark : "",
                            (speed == ElevatorTrip.ElevatorSpeed.X4) ? checkMark : "",
                    });
                }

                // scroll ke bawah
                ioPane.getVerticalScrollBar().setValue(ioPane.getVerticalScrollBar().getMaximum());
                procPane.getVerticalScrollBar().setValue(procPane.getVerticalScrollBar().getMaximum());
            }
        });
    }
}
