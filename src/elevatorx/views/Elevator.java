package elevatorx.views;

import aurelienribon.tweenengine.*;
import aurelienribon.tweenengine.equations.Linear;
import elevatorx.Main;
import elevatorx.model.ElevatorTrip;
import elevatorx.model.TargetFloor;
import elevatorx.extlib.SLAnimator;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import static elevatorx.views.Elevator.ElevatorYPos.ElevatorPosition;

/**
 * Created by Konglie on 7/17/2016.
 */
public class Elevator extends JPanel {
    private final int FloorNum          = 4;
    private final int FloorWidth        = 170;
    private final Font NumberFont       = new Font("", Font.BOLD, 46);
    private final Font TextFont         = NumberFont.deriveFont(18f);
    private final Color ElevatorColor   = Color.DARK_GRAY;
    private final int ElevatorPadding   = 5;
    private final TweenManager tm       = SLAnimator.createTweenManager();
    private final float StandardSpeed   = 2f;

    private ArrayList<TargetFloor> destinations, passengers;
    private ArrayList<ElevatorTrip> trips;

    private float floorHeight = 0f;
    private float elevatorY = -1;
    private int currentFloor = 1;

    private JButton waitingButton;
    private DataTables dataTables;

    private boolean newPassenger = false;

    private enum MovingDirection {
        Up, Down
    }
    private MovingDirection currentDirection = MovingDirection.Up;

    private final Building building;
    public Elevator(Building building){
        super(new MigLayout("insets 0, fillx"));
        this.building = building;
        this.destinations = new ArrayList<TargetFloor>();
        this.passengers = new ArrayList<TargetFloor>();
        this.trips = new ArrayList<ElevatorTrip>();

        buildGUI();
    }

    private void buildGUI(){
        float h = 100 / FloorNum;
        JPanel buttons = new JPanel(new MigLayout("insets 0 10 0 10, gap 0"));
        buttons.setBorder(BorderFactory.createTitledBorder("Permintaan User"));

        final JPanel numbers = new JPanel(new MigLayout("insets 0 10 0 10, fill, wrap 2"));
        numbers.setBorder(BorderFactory.createTitledBorder("Tombol Tujuan"));

        ActionListener btnListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(waitingButton != null){
                    Main.alert("Sedang menunggu input Tombol Tujuan");
                    return;
                }

                JButton btn = (JButton) e.getSource();
                int floor = Integer.parseInt(btn.getClientProperty("floor").toString());
                int direction = Integer.parseInt(btn.getClientProperty("direction").toString());
                if(passengers.size() < 1) {
                    if (direction == 1) {
                       currentDirection = MovingDirection.Up;
                    } else {
                        currentDirection = MovingDirection.Down;
                    }
                }
                btn.setBackground(Color.RED);
                waitingButton = btn;

                queueTo(-1, direction, floor);
//                Main.alert("Tekan tombol Tujuan Anda");
            }
        };

        ActionListener numberListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(waitingButton == null){
                    Main.alert("Tujuan Sudah Ada.");
                    return;
                }

                JButton btn = (JButton) e.getSource();
                int floor = Integer.parseInt(btn.getClientProperty("floor").toString());

                waitingButton.setBackground(null);
                waitingButton = null;

                queueTo(floor, 0, 0);
            }
        };

        for(int i = FloorNum; i > 0; i--){
            JPanel panel = new JPanel(new MigLayout("insets 0, aligny center"));

            int fn = (FloorNum - i) + 1;
            JButton number=  new JButton(fn + "");
            number.addActionListener(numberListener);
            number.putClientProperty("floor", fn);
            numbers.add(number, "h 50, grow");

            // buat tombol naik
            if(i < FloorNum){
                JButton btnUp = new JButton("Naik");
                btnUp.putClientProperty("floor", i);
                btnUp.putClientProperty("direction", 1);
                btnUp.addActionListener(btnListener);
                panel.add(btnUp, "center, wrap, w 100%");
            }

            // buat tombol turun
            if(i > 1){
                JButton btnDown = new JButton("Turun");
                btnDown.putClientProperty("floor", i);
                btnDown.putClientProperty("direction", -1);
                btnDown.addActionListener(btnListener);
                panel.add(btnDown, "center, grow, w 100%");
            }

            buttons.add(panel, "wrap, w 100%, h " + h + "%");
        }

        JPanel elevator = new JPanel(){
            @Override
            public void paintComponent(Graphics g){
                super.paintComponent(g);

                Graphics2D g2 = (Graphics2D) g;
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);

                FontMetrics fm;
                floorHeight = getHeight() / FloorNum;
                g2.setFont(NumberFont);
                g2.setColor(Color.LIGHT_GRAY);
                fm = g2.getFontMetrics();
                int fh = fm.getHeight();
                int fw;
                int ypos, xpos;
                String text;
                for(int i = FloorNum; i > 0; i--){
                    text = i + "";
                    fw = fm.stringWidth(text);
                    ypos = (int) ((FloorNum - i) * floorHeight);
                    xpos = (FloorWidth - fw)/2;

                    // kotak lantai
                    g2.drawRect(0, ypos, FloorWidth, (int) floorHeight);
                    if(i == currentFloor){
                        g2.setColor(Color.CYAN);
                        g2.fillRect(0, ypos, FloorWidth, (int) floorHeight);
                        g2.setColor(Color.LIGHT_GRAY);
                    }

                    ypos += (floorHeight - fh);

                    // nomor lantai
                    g2.drawString(text, xpos, ypos);
                }

                if(elevatorY < 0){
                    elevatorY =  getHeight() - (currentFloor * floorHeight);
                }

                // the elevator
                g2.setColor(ElevatorColor);
                ypos = (int) (elevatorY + ElevatorPadding);
                g2.fillRect(ElevatorPadding, ypos, FloorWidth - (ElevatorPadding*2), (int) floorHeight - (ElevatorPadding*2));

                g2.setColor(ElevatorColor.brighter().brighter().brighter());
                g2.setFont(TextFont);
                text = "Elevator Empty";
                if(currentTrip != null && currentTrip.getPassengerCount() > 0){
                    text = String.format("P: %s, S: %s", currentTrip.getPassengerCount(), currentTrip.getSpeed().toString());
                    g2.setFont(TextFont.deriveFont(14f));
                }
                fm = g2.getFontMetrics();
                fh = fm.getHeight();
                xpos = (FloorWidth - fm.stringWidth(text))/2;
                ypos += (floorHeight - fh) / 2 + fh / 2;
                g2.drawString(text, xpos, ypos);
            }
        };

        add(elevator, "w 180, h 100%");
        add(buttons, "w 150, h 100%");

        JPanel cpanel = new JPanel(new MigLayout("insets 0, fillx"));
        cpanel.add(numbers, "wrap, grow");

        dataTables = new DataTables();
        cpanel.add(dataTables, "grow, wrap");

        add(cpanel, "grow, span, right, push, h 100%");


        // simulation
        buildSimulation(cpanel);
    }

    JToggleButton tbtn;
    private void buildSimulation(JPanel container){
        tbtn = new JToggleButton("Simulate Process");
        container.add(tbtn, "right, push, span");
    }

    public void queueTo(int floor, int dir, int from){
        TargetFloor tf;
        if(floor <= 0) {
            tf = new TargetFloor(floor, dir);
            tf.setFromFloor(from);
            destinations.add(tf);
        } else {
            tf = destinations.get(destinations.size() - 1);
            tf.setToFloor(floor);
            newPassenger = true;
        }

        refreshDataView();
    }

    private void refreshDataView(){
        try {
            dataTables.renderIO(destinations, trips);
        } catch (Exception e){

        }
    }

    // ambil penumpang yang berasal dari lantai ini
    public void collectPassenger(int floor){
        for(Iterator<TargetFloor> it = destinations.iterator(); it.hasNext();){
            TargetFloor tf = it.next();
            if(tf.isWaiting() && tf.getFromFloor() == floor ){
                tf.setMoving();
                passengers.add(tf);
            }
        }
    }

    // cari lantai yang ada penumpangnya
    // apabila terdapat 2 lantai yang berbeda
    // maka akan diambil lantai terdekat terlebih dahulu
    public int waitingFloor(int fromFloor){
        // jarak posisi lift (lantai sekarang) ke lantai tempat penumpang
        int distance = -1;

        // lantai tempat penumpang menunggu
        int wf = 0;

        int currentDistance;
        for(Iterator<TargetFloor> it = destinations.iterator(); it.hasNext(); ){
            TargetFloor tf = it.next();
            if(tf.isMoving() || tf.isArrived()){
                continue;
            }
            currentDistance = (Math.abs(tf.getFromFloor() - fromFloor));
            if(distance < currentDistance){
                distance = currentDistance;
                wf = tf.getFromFloor();
            }
        }

        return wf;
    }


    public float getElevatorY() {
        return elevatorY;
    }

    public void setElevatorY(float elevatorY) {
        this.elevatorY = elevatorY;
        repaint();
    }


    public static class ElevatorYPos implements TweenAccessor<Elevator> {
        public static final int ElevatorPosition = 0xF0;

        @Override
        public int getValues(Elevator elevator, int i, float[] returnValues) {
            if(i != ElevatorPosition){
                return 0;
            }
            returnValues[0] = elevator.getElevatorY();
            return 1;
        }

        @Override
        public void setValues(Elevator elevator, int i, float[] newValues) {
            if(i != ElevatorPosition){
                return;
            }

            elevator.setElevatorY(newValues[0]);
        }
    }

    public void startElevator(){
        // reset animasi pergerakan lift
        tm.killTarget(ElevatorPosition);
        moveTo(currentFloor);
    }

    private ElevatorTrip currentTrip;
    private void moveTo(final int floor){
        if(floorHeight <= 0){
            floorHeight = getHeight() / FloorNum;
        }
        float targetY = getHeight() - (floor * floorHeight);
        float duration = StandardSpeed;
        int passengerCount = passengers.size();
        ElevatorTrip trip = new ElevatorTrip(ElevatorTrip.ElevatorSpeed.Standard, passengerCount);

        // durasi berbanding terbalik dengan kecepatan
        // kecepatan dikali 2 berarti durasi dibagi 2, karena
        // dengan peningkatan kecepatan berarti pengurangan durasi
        if(passengerCount > 4 && passengerCount <= 8){
            // durasi dibagi 2, berarti (menaikkan) kecepatan dikali 2
            duration /= 2;
            trip.setSpeed(ElevatorTrip.ElevatorSpeed.X2);
        } else if(passengerCount > 8){
            duration /= 4;
            trip.setSpeed(ElevatorTrip.ElevatorSpeed.X4);
        }

        // apakah di lantai saat ini ada penumpang baru?
        // ataukah kita hanya "numpang lewat" ?
        if(floor != currentFloor && newPassenger) {
            trips.add(trip);
            currentTrip = trip;
            newPassenger = false;
        }

        // durasi yang dihitung di atas adalah untuk pergerakan 1 lantai
        // sehingga kita perlu hitung kembali
        // pergerakan animasi yang sesuai jumlah lantai yang harus dilalui
        int distance = (Math.abs(currentFloor - floor));
        if(distance > 1){
            duration *= distance;
        }
        Tween.to(this, ElevatorPosition, duration)
            .ease(Linear.INOUT)
            .target(targetY)
            .setCallback(new TweenCallback() {
                @Override
                public void onEvent(int i, BaseTween<?> baseTween) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            currentFloor = floor;
                            boolean beep = false;
                            // turunkan penumpang
                            for(Iterator<TargetFloor> it = passengers.iterator(); it.hasNext(); ){
                                TargetFloor tf = it.next();
                                if(tf.getToFloor() == currentFloor){
                                    if(!beep){
                                        Main.beep();
                                        beep = true;
                                    }
                                    tf.setArrived();
                                    it.remove();
                                }
                            }

                            // ambil penumpang baru, jika ada
                            // ambil dulu penumpang di lantai ini
                            collectPassenger(floor);

                            // jika di lantai ini tidak ada penumpang
                            // maka pergi ke lantai yang ada penumpang
                            if(passengers.size() <= 0){
                                refreshDataView();
                                currentTrip = null;
                                int nextFloor;
                                do {
                                    nextFloor = waitingFloor(currentFloor);
                                    Main.sleep(150);
                                } while (nextFloor <= 0);
                                moveTo(nextFloor);
                                return;
                            }

                            // apabila telah mencapai lantai tertinggi atau terendah
                            // break sejenak
                            int directionBreak = 100;

                            // jika sedang naik dan sudah mencapai lantai tertinggi
                            // balikkan arah menjadi turun
                            if(currentDirection == MovingDirection.Up && currentFloor >= FloorNum){
                                currentDirection = MovingDirection.Down;
                                Main.sleep(directionBreak);
                            }


                            // jika sedang turun dan sudah mencapai lantai terendah
                            // balikkan arah menjadi naik
                            if(currentDirection == MovingDirection.Down && currentFloor <= 1){
                                currentDirection = MovingDirection.Up;
                                Main.sleep(directionBreak);
                            }

                            // naik atau turun tergantung dari arah perjalanan sekarang
                            // menuju lantai lebih rendah atau lebih tinggi
                            int move = (currentDirection == MovingDirection.Up) ? 1 : -1;

                            // jalankan animasi pergerakan lift
                            moveTo(currentFloor + move);
                        }
                    }).start();
                }
            })
            .start(tm);
    }
}
