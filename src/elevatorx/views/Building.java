package elevatorx.views;

import elevatorx.Main;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Konglie on 7/17/2016.
 */
public class Building extends JFrame {
    public Building(){
        super("Elevator X");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(800,600));
        buildGUI();
        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                elevator.startElevator();
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    Main.sleep((int) (Math.random() * 500 + 75));
                    if(!elevator.tbtn.isSelected()){
                        continue;
                    }

                    // jalankan simulasi kegiatan user

                    int to = (int) (Math.random() * 4 + 1);
                    int dir = (Math.random() * 1000%2  == 1) ? 1 : -1;
                    int from = Math.abs(4 - to);
                    if(from == 0){
                        from = 1;
                    }

                    // seolah olah user klik tombol naik/turun
                    elevator.queueTo(-1, dir, from);
                    Main.sleep(130);

                    // kemudian klik nomor lantai tujuan
                    elevator.queueTo(to, dir, from);
                }
            }
        }).start();
    }

    private Elevator elevator;
    private void buildGUI(){
        elevator = new Elevator(this);
        getContentPane().add(elevator, BorderLayout.CENTER);

        JLabel footer = new JLabel("<html><center>Created by Konglie<br/>konglie@kurungkurawal.com</center></html>");
        footer.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        footer.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(footer, BorderLayout.SOUTH);
    }
}
