package elevatorx.extlib;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Aurelien Ribon | http://www.aurelienribon.com/
 */
public class SLAnimator {
    private static final List<TweenManager> tweenManagers = new ArrayList<TweenManager>();
    private static boolean running = false;

    static {
        Tween.setCombinedAttributesLimit(4);
    }

    public static TweenManager createTweenManager() {
        TweenManager tm = new TweenManager();
        tweenManagers.add(tm);
        return tm;
    }

    public static void start() {
        running = true;

        Runnable runnable = new Runnable() {@Override public void run() {
            long lastMillis = System.currentTimeMillis();
            long deltaLastMillis = System.currentTimeMillis();

            while (running) {
                long newMillis = System.currentTimeMillis();
                long sleep = 15 - (newMillis - lastMillis);
                lastMillis = newMillis;

                if (sleep > 1)
                    try {Thread.sleep(sleep);} catch (InterruptedException ex) {}

                long deltaNewMillis = System.currentTimeMillis();
                final float delta = (deltaNewMillis - deltaLastMillis) / 1000f;

                try {
                    SwingUtilities.invokeAndWait(new Runnable() {@Override public void run() {
                        for (int i=0, n=tweenManagers.size(); i<n; i++) tweenManagers.get(i).update(delta);
                    }});
                } catch (InterruptedException ex) {
                } catch (InvocationTargetException ex) {
                }

                deltaLastMillis = newMillis;
            }
        }};

        new Thread(runnable).start();
    }

    public static void stop() {
        running = false;
    }
}