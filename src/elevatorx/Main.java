package elevatorx;

import aurelienribon.tweenengine.Tween;
import elevatorx.extlib.SLAnimator;
import elevatorx.views.Building;
import elevatorx.views.Elevator;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Konglie on 7/17/2016.
 */
public class Main {
    public static JFrame mainFrame;

    public static void main(String[] args){
        SLAnimator.start();
        Tween.registerAccessor(Elevator.class, new Elevator.ElevatorYPos());

        try{
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e){

        }

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                mainFrame = new Building();
            }
        });
    }

    public static void out(Object o){
        System.out.println(o);
    }

    public static void alert(String s){
        JOptionPane.showMessageDialog(mainFrame, s, "Informasi", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void sleep(int ms){
        try{
            Thread.sleep(ms);
        } catch (Exception e){

        }
    }

    public static void beep(){
        Toolkit.getDefaultToolkit().beep();
    }
}
