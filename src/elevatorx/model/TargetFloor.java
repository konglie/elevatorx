package elevatorx.model;

/**
 * Created by Konglie on 7/17/2016.
 */
public class TargetFloor {
    public int getFromFloor() {
        return fromFloor;
    }

    public void setFromFloor(int fromFloor) {
        this.fromFloor = fromFloor;
    }

    public enum STATE {
        Waiting, Moving, Arrived
    }

    private STATE state;
    private int toFloor = 0;
    private int direction;
    private int fromFloor;
    public TargetFloor(int to, int direction){
        toFloor = to;
        state = null;
        this.direction = direction;
    }

    public boolean isArrived() {
        return state == STATE.Arrived;
    }

    public boolean isWaiting(){
        return toFloor > 0 && state == STATE.Waiting;
    }

    public boolean isMoving(){
        return state == STATE.Moving;
    }

    public void setMoving(){
        state = STATE.Moving;
    }

    public void setArrived(){
        state = STATE.Arrived;
    }

    public int getToFloor(){
        return toFloor;
    }

    public void setToFloor(int f){
        this.toFloor = f;
        this.state = STATE.Waiting;
    }

    public int getDirection(){
        return direction;
    }
}
