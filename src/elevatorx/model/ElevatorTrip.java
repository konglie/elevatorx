package elevatorx.model;

/**
 * Created by Konglie on 7/17/2016.
 */
public class ElevatorTrip {

    public enum ElevatorSpeed {
        Standard, X2, X4
    }
    private ElevatorSpeed speed;
    private int passengerCount;
    public ElevatorTrip(ElevatorSpeed speed, int passengerCount){
        this.speed = speed;
        this.passengerCount = passengerCount;
    }

    public ElevatorSpeed getSpeed() {
        return speed;
    }

    public void setSpeed(ElevatorSpeed speed) {
        this.speed = speed;
    }

    public int getPassengerCount() {
        return passengerCount;
    }

    public void setPassengerCount(int passengerCount) {
        this.passengerCount = passengerCount;
    }
}
